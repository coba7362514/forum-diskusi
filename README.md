## Final Project

## Anggota Kelompok

-   Muhammad Fahri Ramadhan (@fahri4321)
-   Sri Oktari (@srioktari)
-   Azwar Rusli (@azwarrr29)
-   Andre (@andresmks00)
-   Revelino Fitra Fahmi (@Ravelinoo)

## Tema Project

Forum Disku

## ERD

<p align="center"><img src="public/images/erd dsiku.png"></p>

## Link Video

-   Link Google Drive Video Aplikasi : [https://drive.google.com/drive/folders/1CBxqpKgigWCB3NfECnm7T1Tm3jQjPEdx?usp=share_link](https://drive.google.com/drive/folders/1CBxqpKgigWCB3NfECnm7T1Tm3jQjPEdx?usp=share_link).
-   Link Deploy : [https://disku.ezscode.com/](https://disku.ezscode.com/).
